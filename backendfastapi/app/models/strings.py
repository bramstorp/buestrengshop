from tortoise.models import Model 
from tortoise import fields 
from passlib.hash import bcrypt


class String(Model):
    id = fields.IntField(pk=True)
    string_type = fields.CharField(255)
    string_length = fields.IntField()
    winding_a = fields.IntField()
    winding_b = fields.IntField()
    winding_c = fields.IntField()
    winding_d = fields.IntField()
    winding_e = fields.IntField()
    winding_f = fields.IntField()
    eye_x = fields.IntField()
    eye_y = fields.IntField()
    material = fields.CharField(255)
    string_amount = fields.IntField()
    color = fields.CharField(255)
    twist = fields.FloatField()

