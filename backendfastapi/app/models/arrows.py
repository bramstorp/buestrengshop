from tortoise.models import Model 
from tortoise import fields 
from passlib.hash import bcrypt


class Arrow(Model):
    id = fields.IntField(pk=True)
    arrow_name = fields.CharField(255)
    arrow_length = fields.IntField()
    arrow_amount = fields.IntField()
    wrap = fields.BooleanField(default=True)
    wrap_color = fields.CharField(255)
    venis_type = fields.CharField(255)
    venis_length = fields.CharField(255)
    venis_color = fields.CharField(255)
    point_type = fields.CharField(255)
    point_weight = fields.IntField()
    insert_type = fields.CharField(255)
    nock_type = fields.CharField(255)
    nock_color = fields.CharField(255)    
    nock_insert_type = fields.CharField(255)

