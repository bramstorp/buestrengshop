from typing import List
from fastapi import APIRouter

from ..models.strings import String
from ..schemas.strings import String_Pydantic, CreateString_Pydantic

router = APIRouter(
    tags=["strings"],
    responses={404: {"description": "Not found"}},
)

@router.get('/strings', response_model=List[String_Pydantic])
async def get_strings():
    return await String_Pydantic.from_queryset(String.all())  

@router.get('/string/{string_id}', response_model=String_Pydantic)
async def get_strings(string_id: int):
    return await String_Pydantic.from_queryset_single(String.get(id=string_id))

@router.post('/create-string', response_model=String_Pydantic)
async def create_string(string: CreateString_Pydantic):
    string_obj = await String.create(**string.dict(exclude_unset=True))
    return await CreateString_Pydantic.from_tortoise_orm(string_obj)