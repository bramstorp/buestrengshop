from typing import List
from fastapi import APIRouter

from ..models.arrows import Arrow
from ..schemas.arrows import Arrow_Pydantic, CreateArrow_Pydantic

router = APIRouter(
    tags=["arrows"],
    responses={404: {"description": "Not found"}},
)

@router.get('/arrows', response_model=List[Arrow_Pydantic])
async def get_arrows():
    return await Arrow_Pydantic.from_queryset(String.all())  

@router.get('/arrow/{arrow_id}', response_model=Arrow_Pydantic)
async def get_arrow(arrow_id: int):
    return await Arrow_Pydantic.from_queryset_single(String.get(id=arrow_id))

@router.post('/create-arrow', response_model=Arrow_Pydantic)
async def create_arrow(arrow: CreateArrow_Pydantic):
    arrow_obj = await Arrow.create(**arrow.dict(exclude_unset=True))
    return await CreateArrow_Pydantic.from_tortoise_orm(arrow_obj)