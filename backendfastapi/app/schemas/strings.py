from tortoise.contrib.pydantic import pydantic_model_creator
from ..models.strings import String

String_Pydantic = pydantic_model_creator(String, name='String')
CreateString_Pydantic = pydantic_model_creator(String, name='CreateString')