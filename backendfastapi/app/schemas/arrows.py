from tortoise.contrib.pydantic import pydantic_model_creator
from ..models.arrows import Arrow

Arrow_Pydantic = pydantic_model_creator(Arrow, name='Arrow')
CreateArrow_Pydantic = pydantic_model_creator(Arrow, name='CreateArrow')