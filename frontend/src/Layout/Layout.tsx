import React, { FunctionComponent } from "react";
import { Navbar } from "./Navbar";
import "bootstrap/dist/css/bootstrap.min.css";
import PropTypes from "prop-types";

export const Layout: FunctionComponent = (props) => (
  <div>
    <Navbar />
    <div className="container">{props.children}</div>
  </div>
);

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};
