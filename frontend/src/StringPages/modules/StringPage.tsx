import React, { FunctionComponent, useState } from "react";
import { Formik, Form } from "formik";

import { StringFormField } from "./StringFormField";
import { createStrings } from "../http/string.http";
import { SpinnerLoading } from "../../shared/SpinnerLoading";

interface IStringForm {
  string_type: string;
  string_length: number;
  winding_a: number;
  winding_b: number;
  winding_c: number;
  winding_d: number;
  winding_e: number;
  winding_f: number;
  eye_x: number;
  eye_y: number;
  material: string;
  string_amount: number;
  color: string;
  twist: number;
}

const formLabelTrans = (label: string) => {
  switch (label) {
    case "string_type":
      return "Streng Type";
    case "string_length":
      return "Streng Længende";
    case "winding_a":
      return "Winding A";
    case "winding_b":
      return "Winding B";
    case "winding_c":
      return "Winding C";
    case "winding_d":
      return "Winding D";
    case "winding_e":
      return "Winding E";
    case "winding_f":
      return "Winding F";
    case "eye_x":
      return "Eye X";
    case "eye_y":
      return "Eye Y";
    case "material":
      return "Martriale";
    case "string_amount":
      return "Streng Antal";
    case "color":
      return "Farve";
    case "twist":
      return "Twist";
  }
};

export const StringForm: FunctionComponent = () => {
  const [loading, setLoading] = useState(false);

  const initialValues: IStringForm = {
    string_type: "",
    string_length: 0,
    winding_a: 0,
    winding_b: 0,
    winding_c: 0,
    winding_d: 0,
    winding_e: 0,
    winding_f: 0,
    eye_x: 0,
    eye_y: 0,
    material: "",
    string_amount: 0,
    color: "",
    twist: 0.0,
  };
  return (
    <div>
      <h1>Streng Form</h1>
      <Formik
        initialValues={initialValues}
        onSubmit={(values, actions) => {
          createStrings(values)
            .then(() => {
              setLoading(true);
            })
            .then(() => {
              setLoading(false);
            });
        }}
      >
        <Form>
          <div className="row pb-3">
            {Object.entries(initialValues).map(([key, value]) => {
              return <StringFormField key={key} type={typeof value} name={key} label={formLabelTrans(key)} />;
            })}
          </div>
          {loading ? (
            <SpinnerLoading loading={loading} color={"#FF0000"} />
          ) : (
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          )}
        </Form>
      </Formik>
    </div>
  );
};
