import React, { FunctionComponent } from "react";
import { Field } from "formik";

interface IStringFormFieldProps {
  label?: string;
  type?: string;
  name?: string;
}

export const StringFormField: FunctionComponent<IStringFormFieldProps> = ({ label, type, name }) => {
  return (
    <div className="form-group col-4 pt-3">
      <label className="form-label">{label}</label>
      <Field className="form-control" type={type} id="firstName" name={name} placeholder={label} />
    </div>
  );
};
