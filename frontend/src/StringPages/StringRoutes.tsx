import React, { FunctionComponent } from "react";
import { Route, Switch, useRouteMatch } from "react-router-dom";

import { String } from "./routes/String";

export const StringRoutes: FunctionComponent = () => {
  const { url } = useRouteMatch();

  return (
    <Switch>
      <Route path={`${url}`} exact={true} component={String} />
    </Switch>
  );
};
