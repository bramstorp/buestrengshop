import axios from "axios";

interface IString {
  string_type: string;
  string_length: number;
  winding_a: number;
  winding_b: number;
  winding_c: number;
  winding_d: number;
  winding_e: number;
  winding_f: number;
  eye_x: number;
  eye_y: number;
  material: string;
  string_amount: number;
  color: string;
  twist: number;
}

export const createStrings = async (values: IString) => {
  const bodyData = JSON.stringify(values, null, 2);
  try {
    const response = axios({
      method: "post",
      url: "http://localhost:8000/create-string/",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      data: bodyData,
    });
    return await response;
  } catch (err) {
    console.error(err);
  }
};
