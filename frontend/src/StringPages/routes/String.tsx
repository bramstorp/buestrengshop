import React, { FunctionComponent } from "react";

import { StringForm } from "../modules/StringPage";

export const String: FunctionComponent = () => {
  return <StringForm />;
};
