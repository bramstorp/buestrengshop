import React, { FunctionComponent } from "react";
import ClipLoader from "react-spinners/ClipLoader";

interface ISpinnerLoading {
  color?: string;
  override?: string;
  loading?: boolean;
  size?: number;
}

export const SpinnerLoading: FunctionComponent<ISpinnerLoading> = ({ color, loading, override, size }) => (
  <ClipLoader color={color} loading={loading} css={override} size={size} />
);
